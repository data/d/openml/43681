# OpenML dataset: Predicting-Critical-Heat-Flux

https://www.openml.org/d/43681

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was prepared for the journal article entitled "On the prediction of critical heat flux using a physics-informed machine learning-aided framework" (doi: 10.1016/j.applthermaleng.2019.114540). The dataset contains processed and compiled records of experimental critical heat flux and boundary conditions used for the work presented in the article. 
Acknowledgements
Zhao, Xingang (2020), Data for: On the prediction of critical heat flux using a physics-informed machine learning-aided framework, Mendeley Data, V1, doi: 10.17632/5p5h37tyv7.1

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43681) of an [OpenML dataset](https://www.openml.org/d/43681). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43681/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43681/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43681/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

